import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


@pytest.mark.parametrize(
    "file",
    [
        "/etc/GeoIP.conf",
        "/etc/systemd/system/geoipupdate.service",
        "/etc/systemd/system/geoipupdate.timer",
        "/var/lib/GeoIP/GeoLite2-City.mmdb",
        "/var/lib/GeoIP/GeoLite2-ASN.mmdb",
        "/var/lib/GeoIP/GeoLite2-Country.mmdb",
    ],
)
def test_files(host, file):
    f = host.file(file)
    assert f.exists


def test_service(host):
    service = host.service("geoipupdate.service")
    assert service.is_enabled


def test_timer(host):
    timer = host.service("geoipupdate.timer")
    assert timer.is_enabled
    assert timer.is_running
